from flask import Flask, render_template, request,make_response
from flask_restful import Api, Resource
from flask_cors import CORS, cross_origin
import mysql.connector
import time
import datetime

cnx = mysql.connector.connect(host='127.0.0.1', database='test',user='root', password='', port=3306, connect_timeout=10000)
mycursor = cnx.cursor()
app = Flask(__name__)
api = Api(app)
cors = CORS(app)
#app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/')
@cross_origin()
def student():
    '''
        returns the index page
    '''
    return render_template('index.html') #calling index page

@cross_origin()
def currentTime():
        '''
            returns the current time stamp
        '''
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        return timestamp

@cross_origin()
def checkAndInsert(userName, emailId, phoneNo, password, Updated_on):
        '''
            Insert and update the user
        '''
        sql = "select * from userData where emailId = %s"
        e_id = emailId
        var_email =( emailId, )
        mycursor = cnx.cursor() #cursor reconnection
        mycursor.execute(sql, var_email)
        records = mycursor.fetchall()
        if len(records) < 1:
            sql = "INSERT INTO userData (userName, emailId, phoneNo, password, Updated_on) VALUES (%s, %s, %s, %s, %s)"
            val = (userName, emailId, phoneNo, password, Updated_on)
            mycursor.execute(sql, val)
            cnx.commit()
            mycursor.close()
            #cnx.close()       
            print("new data added")
            return 0
        else:
            sql = "UPDATE userData SET userName =%s, emailId =%s, phoneNo =%s, password =%s, Updated_on =%s where emailId = %s "
            val = (userName, emailId, phoneNo, password, Updated_on, e_id)
            mycursor.execute(sql, val)
            cnx.commit()
            mycursor.close()
            #cnx.close()  
            print("User already Exist")
            return 1

@cross_origin()
def search(email_id):
    '''
        Search User
    '''
    mycursor = cnx.cursor() #cursor reconnection
    sql = "select * from userData where emailId = %s" 
    val = (email_id,)
    mycursor.execute(sql,val)
    records = mycursor.fetchall()
    if(len(records)) > 0: #checking whether record is null or not 
        flag = 1
        dic = {"flag":flag,"name":records[0][1],"email":records[0][2],"phone":records[0][3],"pwd":records[0][4],"time":records[0][5]}
    else:
        flag = 0
        dic = {"flag":flag}
    cnx.commit()
    mycursor.close()
    return dic

@cross_origin()
def delete(email_id):
    '''
        Delete User
    '''
    mycursor = cnx.cursor() #cursor reconnection
    sql = "select * from userData where emailId = %s"
    val = (email_id,)
    mycursor.execute(sql,val)
    records = mycursor.fetchall()
    if len(records) < 1: #checking if user exist or not
        flag =0
        return flag
    else:
        sql = "delete from userData where emailId = %s"
        val =(email_id,)
        mycursor.execute(sql,val)
        cnx.commit()
        mycursor.close()  
        flag = 1
        return flag


class InsertAPI(Resource):
    '''
        API to create a new user and list a given user detail
    '''
    @cross_origin()
    def post(self):
        name = request.form.get("name")
        password = request.form.get("pwd")
        email = request.form.get("email")
        phone =request.form.get("phone")
        times = currentTime()
        flag = checkAndInsert(name, email, phone, password, times)
        dic = search(email)
        return dic
        #return ('data inserted', dic)
        # if(flag==0):
        #     return make_response(render_template('index.html',alert="User data added"))
        # else:
        #     return make_response(render_template('index.html',alert="User data updated"))
      
    def get(self):
        s = request.args.get('email1')
        #email = request.form.get("email1")
        #print("id is :",email)
        dic = search(s)
        #return make_response(render_template('second.html',data = dic)) 
        return dic

class DeleteAPI(Resource):
    '''
        API to delete a user
    '''
    def post(self):
        email = request.form.get("email1")
        flag = delete(email)
        if flag == 0:
            return make_response(render_template('index.html',alert="No User Found")) 
        else:
            return make_response(render_template('index.html',alert="User Data Deleted")) 

api.add_resource(InsertAPI, '/insert', endpoint = 'insert') #for insertion
api.add_resource(InsertAPI, '/search') #for search
api.add_resource(DeleteAPI, '/delete', endpoint = 'delete') #for delete

if __name__ == '__main__':
   app.run(debug = True)